function createNode(element) {
  return document.createElement(element);
}
function append(parent, el) {
  return parent.appendChild(el);
}

const ul = document.getElementById("people");

fetch("https://jsonplaceholder.typicode.com/users")
  .then((response) => response.json())
  .then((data) => {
    let people = data;
    (function () {
      for (let i = 0; i < people.length; i++) {
        let li = createNode("li");
        let span = createNode("span");

        li.innerHTML = people[i].name;
        span.innerHTML = people[i].email;

        append(li, span);
        append(ul, li);
      }
    })();
  });
